<?php namespace Wpstudio\Helpers;

use Backend;
use System\Classes\PluginBase;

/**
 * helpers Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'helpers',
            'description' => 'No description provided yet...',
            'author'      => 'wpstudio',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Wpstudio\Helpers\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'wpstudio.helpers.some_permission' => [
                'tab' => 'helpers',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'helpers' => [
                'label'       => 'helpers',
                'url'         => Backend::url('wpstudio/helpers/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['wpstudio.helpers.*'],
                'order'       => 500,
            ],
        ];
    }
}
