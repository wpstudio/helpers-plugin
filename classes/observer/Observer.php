<?php namespace Wpstudio\Helpers\Classes\Observer;

interface Observer
{
    public static function getClass(): string;
}
